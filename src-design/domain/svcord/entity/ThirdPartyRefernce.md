# Description
*Please add information that is helpful to understand the business context and the relevance.*

<br />

## Constructor operation
*The Construct operation is used to construct a local instance of the external entity and initialize the associated properties with certain prespecified values that are provided as input properties.*

<br />

## Load operation
*The Load operation is used to load the external instance. This is normally done by calling a REST integration service of the integration namespace.*

<br />

## Validate operation
*The Validate operation gives information regarding the existence of a specific instance of an External Entity. It can also be used for updating the local instance of an External Entity.*

<br />